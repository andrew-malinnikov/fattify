import Vue from 'vue'

Vue.component('Diary', require('./../Components/NutritionDiary/Diary.vue').default)
Vue.component('DiaryDate', require('./../Components/NutritionDiary/DiaryDate.vue').default)
Vue.component('DropdownMenu', require('./../Components/DropdownMenu.vue').default)
