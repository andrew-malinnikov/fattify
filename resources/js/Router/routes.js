import Diary from './../Pages/Diary'
import Login from './../Pages/Auth/Login.vue'
import Register from './../Pages/Auth/Register.vue'
import Logout from './../Pages/Auth/Logout.vue'

export default {
  mode: 'history',
  routes: [
    {path: '/my-diary', component: Diary, name: 'my-diary', meta: { auth: true }},

    // Auth
    {path: '/login', component: Login, name: 'auth.show-login-form'},
    {path: '/register', component: Register, name: 'auth.show-register-form'},
    {path: '/logout', component: Logout, name: 'logout'},
  ]
}
