@extends('layouts.landing')
@section ('title', 'Fattify — Считай калории быстро и удобно')

@section('content')
    <x-landing.elements.section>
        <x-landing.navigation />
    </x-landing.elements.section>
    <x-landing.hero />
    <x-landing.how-it-works />
    <x-landing.search-for-product />
    <x-landing.footer />
@endsection
