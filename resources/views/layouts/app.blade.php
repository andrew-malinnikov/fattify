@extends('layouts.base')

@section('body')
    <div class="bg-gray-100">
        <x-app.header />
        <x-app.elements.section class="rounded-lg px-4 pb-8 lg:px-0">
            <div class="px-4 py-8 -mt-20 lg:-mt-40 bg-white rounded-lg lg:px-10 lg:py-12 shadow-md">
                @yield('content')
            </div>
        </x-app.elements.section>
    </div>
@endsection
