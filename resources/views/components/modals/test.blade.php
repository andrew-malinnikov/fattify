@props([
    'date',
    'mealTime',
])

<div>
    <x-modals.modal>
        {{ $mealTime }}
        <livewire:app.diary.write
          :date="$date"
          :meal-time="$mealTime"
        />
    </x-modals.modal>

</div>
