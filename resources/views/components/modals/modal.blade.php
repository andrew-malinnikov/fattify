@props([
    'name' => session('show-modal'),
])
<div>
    @if (session('show-modal') && session('show-modal') == $name)
        <div
          x-data="{show: true }"
          x-init="
              document.querySelector('body').classList.add('overflow-hidden', 'fixed', 'h-full', 'w-full')
            window.addEventListener('keyup', (e) => {
              if (e.key == 'Escape') {
                $dispatch('close-modal')
              }
            })
          "
          x-on:close-modal.window="$el.remove(); document.querySelector('body').classList.remove('overflow-hidden', 'fixed', 'h-full', 'w-full')"
        >
          <div
            x-show="show"
            x-description="Make new diary entry"
            x-transition:enter="transform ease-out duration-300 transition"
            x-transition:enter-start="translate-y-2 opacity-0"
            x-transition:enter-end="translate-y-0 opacity-100 sm:translate-x-0"
            x-transition:leave="transition ease-in duration-100"
            x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            class="fixed px-4 pb-4 inset-0 flex items-center justify-center z-10"
            style="display: none;"
          >
            <div class="fixed inset-0 transition-opacity">
              <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
            </div>

            <div
              x-on:click.away="$dispatch('close-modal')"
              class="relative bg-white rounded-lg shadow-xl transform transition-all md:max-w-lg w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline"
            >
                <div class="absolute top-0 right-0 p-4">
                    <button>
                        <x-icons.x-circle x-on:click.prevent="$dispatch('close-modal')" class="h-6 w-6 fill-current text-green-500 transition duration-300 hover:text-green-400" />
                    </button>
                </div>

                {{ $slot }}
            </div>
          </div>
        </div>
    @endif
</div>

