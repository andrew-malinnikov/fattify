@props([
    'route' => null,
])

@php
$isActive = $route == request()->route()->getName();
@endphp
<li class="whitespace-no-wrap flex flex-col justify-center">
    <a 
        href="{{ $route ? route($route) : '#' }}" 
        class="
            inline-block py-2 px-4 
            text-sm  text-gray-500
            {{ $isActive ? 'border-b-2 border-green-400 text-green-400 font-semibold' : '' }}
        "
    >
        {{ $slot }}
    </a>
</li>
