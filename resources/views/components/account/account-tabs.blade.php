<div class="relative">
    <div class="overflow-x-auto scrolling-auto">
        <ul class="flex space-x-4 border-b border-gray-200">
            <x-account.account-tab-item route="account">My Info</x-account.account-tab-item>
            <x-account.account-tab-item route="goal">My Goal</x-account.account-tab-item>
            <x-account.account-tab-item>Reports</x-account.account-tab-item>
            <x-account.account-tab-item>Progress</x-account.account-tab-item>
            <x-account.account-tab-item>something else</x-account.account-tab-item>
            <x-account.account-tab-item>just this</x-account.account-tab-item>
        </ul>
    </div>
</div>
