<div>
    <div x-data="{isOpen: false}" class="relative">
        <button
            x-on:click="isOpen = ! isOpen"
            class="block"
        >
            <x-avatar />
        </button>
        <button
            x-on:click="isOpen = false"
            x-show="isOpen"
            class="fixed inset-0 h-full w-full"
        ></button>
        <div
            x-show.transition.duration.300ms="isOpen"
            style="display: none;"
            class="absolute z-10 mt-2 py-2 w-40 bg-green-700 right-0 shadow-xl rounded-lg"
        >
            <x-dropdown-menu-item :to="route('my-diary')">My Diary</x-dropdown-menu-item>
            <x-dropdown-menu-item :to="route('account')">Account</x-dropdown-menu-item>
            <div class="border-b border-green-200"></div>
            <x-dropdown-menu-item :to="route('logout')">Logout</x-dropdown-menu-item>
        </div>
    </div>
</div>
