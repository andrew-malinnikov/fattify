<x-app.elements.section>
    <div class="py-6 px-4">
      <div class="font-semibold flex items-center justify-center">
        <button
            wire:click="$set('date', '{{Carbon\Carbon::parse($date)->subDay()}}')"
            class="px-2 focus:outline-none focus:shadow-outline"
        >
            <x-icons.double-chevron-left class="stroke-current w-5 h-5 text-green-500" stroke-width="2" />
        </button>
        <x-form.date-picker wire:model="date">
            <x-slot name="trigger">
                <button class="flex items-center focus:outline-none focus:shadow-outline">
                    <x-icons.calendar class="stroke-current w-5 h-5 text-green-500" stroke-width="2" />
                    <span class="ml-2 font-bold text-lg text-gray-800">{{ $this->formattedDate }}</span>
                    </button>
                </x-slot>
            </x-form.date-picker>
            <button
                wire:click="$set('date', '{{Carbon\Carbon::parse($date)->addDay()}}')"
                class="px-2 focus:outline-none focus:shadow-outline"
            >
                <x-icons.double-chevron-right class="stroke-current w-5 h-5 text-green-500" stroke-width="2" />
            </button>

        </div>
    </div>
</x-app.elements.section>
