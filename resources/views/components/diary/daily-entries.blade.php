<x-app.elements.section class="py-6">
    <div class="space-y-6 lg:px-0">
        @foreach ($dailyResults as $mealTime => $diaryEntries)
        <div>
            <div x-data="{expandEntries: false }">
                <div class="bg-white rounded-lg shadow overflow-hidden  ">
                    <div class="py-6 flex justify-between items-center px-4">
                        <div class="font-bold text-green-500 uppercase text-xs tracking-wider">{{ $mealTime }} </div>
                        <div class="flex items-center">
                            <span class="mr-6">{{ $diaryEntries->groupByMacros()->calorie }} <span class="text-gray-600">cal.</span></span>
                            <button wire:click="showWriteToDiaryModal('{{$mealTime}}')">
                                <x-icons.plus-square class="w-6 h-6 stroke-current text-green-600" />
                            </button>
                        </div>
                    </div>
                    @include ('app.diary._meal-time-overview')
                    @include ('app.diary._meal-time-diary-entries')
                </div>
                @include ('app.diary._meal-time-diary-entries-toggle')
            </div>
        </div>
        @endforeach
    </div>
</x-app.elements.section>
