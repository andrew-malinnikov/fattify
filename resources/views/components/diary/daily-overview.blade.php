<x-app.elements.section>
    <div class="shadow-xl rounded-md overflow-hidden">
        <div class="py-10 px-4 bg-green-400 text-center">
            <div class="text-5xl font-black text-white">{{ $totalDailyConsumption->calorie }}</div>
            <h1 class="text-green-200 font-bold text uppercase">Your calories today</h1>
        </div>
        <div class="flex py-6 bg-green-500">
            <div class="flex-1 flex flex-col items-center">
                <h4 class="text-green-200 uppercase text-sm tracking-wide font-semibold">Fat</h4>
                <span class="text-green-100 text-xl font-bold">{{ $totalDailyConsumption->fat }} g</span>
            </div>
            <div class="flex-1 flex flex-col items-center">
                <h4 class="text-green-200 uppercase text-sm tracking-wide font-semibold">Carbohydrate</h4>
                <span class="text-green-100 text-xl font-bold">{{ $totalDailyConsumption->carbohydrates }} g</span>
            </div>
            <div class="flex-1 flex flex-col items-center">
                <h4 class="text-green-200 uppercase text-sm tracking-wide font-semibold">Protein</h4>
                <span class="text-green-100 text-xl font-bold">{{ $totalDailyConsumption->protein }} g</span>
            </div>
        </div>
    </div>
</x-app.elements.section>
