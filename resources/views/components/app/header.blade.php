<div>
    <x-app.elements.section class="bg-green-200 border-b border-green-100 ">
        <div class="h-20 flex items-center justify-between px-4 lg:px-0">
            <div>
                <a href="{{route('home')}}">
                    <x-logo class="h-8 w-auto" />
                </a>
            </div>
            <x-dropdown-menu></x-dropdown-menu>
        </div>
    </x-app.elements.section>

    <x-app.elements.section class="bg-green-200">
        <div class="pt-8 pb-32 lg:pt-20 lg:pb-48 px-4 lg:px-0">
            <div>
                <h1 class="font-bold text-3xl text-green-900">
                    @yield('heading', 'My Diary')
                </h1>
            </div>
        </div>
    </x-app.elements.section>
</div>
