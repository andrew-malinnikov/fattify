@props([
    'color' => 'white'
])
<section {{$attributes}}>
    <div class="max-w-2xl lg:max-w-5xl mx-auto">
        {{$slot}}
    </div>
</section>
