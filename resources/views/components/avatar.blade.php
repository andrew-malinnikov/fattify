<div>
    @auth
        <img 
            class="h-10 focus:outline-none rounded-full" 
            src="{{ auth()->user()->avatarUrl() }}" 
            alt="Avatar picture"
        >
    @else
        <div class="p-2 border-2 border-green-400 focus:border-green-700 focus:outline-none rounded-full">
            <x-icons.user class="w-6 stroke-current text-green-700" />
        </div>
    @endauth
</div>
