<x-landing.elements.section class="py-16 mt-16 min-h-screen">
    <main class="px-4 lg:px-0">
        <div class="lg:flex lg:space-x-20">
            <div>
                <div class="smartphone mx-auto shadow-2xl">
                  <div class="content">
                    <video class="h-full w-full top-0 object-cover"autoplay muted loop>
                      <source src="/img/how-to-use.mov" type="video/mp4">
                    </video>
                  </div>
                </div>
                <x-landing.elements.cta class="hidden lg:block">Попробуйте Fattify бесплатно</x-landing.elements.cta>
            </div>

            <div>
                <div class="mt-10 prose prose-lg">
                    <h3>Быстро и удобно —</h3>
                    <div>
                        <p>Не нужно искать продукт в каталоге — сразу записывайте его БЖУ.</p>
                    </div>

                    <div>
                        <p>С Fattify вы не "забьете", потому что это легко.</p>
                    </div>
                </div>

                <div>
                    <div>
                        <div class="prose">
                            <h3>Без Fattify</h3>
                        </div>
                        <div class="flex flex-col">
                            <div class="px-4 py-3 h-24 rounded-md shadow-md border border-gray-200 bg-white flex items-center">
                                <div class="w-5/6 text-xl">Тратите время на поиск продукта в каталоге</div>
                                <div class="w-1/6 flex justify-end"><span class="px-1 py-1 bg-red-100 rounded-full"><svg class="text-red-700 w-6 h-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M6 18L18 6M6 6l12 12"></path></svg></span></div>
                            </div>
                            <div class="px-4 py-3 h-24 rounded-md shadow-md border border-gray-200 bg-white flex items-center">
                                <div class="w-5/6 text-xl">Не всегда находите нужный вам продукт</div>
                                <div class="w-1/6 flex justify-end"><span class="px-1 py-1 bg-red-100 rounded-full"><svg class="text-red-700 w-6 h-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M6 18L18 6M6 6l12 12"></path></svg></span></div>
                            </div>
                            <div class="px-4 py-3 h-24 rounded-md shadow-md border border-gray-200 bg-white flex items-center">
                                <div class="w-5/6 text-xl">Ищете продукт с похожим БЖУ, тратите время</div>
                                <div class="w-1/6 flex justify-end"><span class="px-1 py-1 bg-red-100 rounded-full"><svg class="text-red-700 w-6 h-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M6 18L18 6M6 6l12 12"></path></svg></span></div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-12">
                        <div class="prose">
                            <h3>С Fattify</h3>
                        </div>
                        <div class="flex flex-col">
                            <div class="px-4 py-3 h-24 rounded-md shadow-md border border-gray-200 bg-white flex items-center">
                                <div class="w-5/6 text-xl">Сразу записываете, сколько макронутриентов вы съели.</div>
                                <div class="w-1/6 flex justify-end"><span class="px-1 py-1 bg-green-100 rounded-full"><svg class="text-green-700 w-6 h-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M5 13l4 4L19 7"></path></svg></span></div>
                            </div>
                            <div class="px-4 py-3 rounded-md shadow-md border border-gray-200 bg-white flex items-center">
                                <div>
                                    <div class="w-5/6 text-xl">Не ищете продукт в каталоге, потому что это не важно</div>
                                    <div class="text-gray-600">Есть возможность указать, что за продукт. Но это необязательно.</div>
                                </div>
                                <div class="w-1/6 flex justify-end"><span class="px-1 py-1 bg-green-100 rounded-full"><svg class="text-green-700 w-6 h-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M5 13l4 4L19 7"></path></svg></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <x-landing.elements.cta class="lg:hidden">Попробуйте Fattify бесплатно</x-landing.elements.cta>
    </main>
</x-landing.elements.section>
