<div class="relative pt-6 px-4 sm:px-6 lg:px-8">
    <nav class="relative flex items-center justify-between sm:h-10">
        <div class="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
            <div class="flex items-center justify-between w-full md:w-auto">
                <a href="#" aria-label="Home">
                    <svg class="h-8 w-auto sm:h-10" viewBox="0 0 181 184" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M160.075 26.6695C146.393 13.4265 128.803 4.61821 109.731 1.45929C90.659 -1.69963 71.0442 0.946391 53.5913 9.0325C36.1385 17.1186 -18.4659 16.0491 12.2878 46.6059C43.0414 77.1627 -1.07562 81.7503 1 100.371C3.07562 118.991 6.04142 178.749 23.8764 150.568C41.7115 122.387 53.6765 174.452 72.5097 178.749C91.3429 183.045 103.041 145.163 129.026 174.557C155.011 203.951 162.207 155.296 172.656 139.533C172.656 139.533 101.921 108.325 93.9605 90.6627C86 73 160.075 26.6695 160.075 26.6695Z" fill="#48BB78"/>
                        <circle cx="66" cy="123" r="12" fill="#C4C4C4"/>
                        <circle cx="169" cy="91" r="12" fill="#48BB78"/>
                    </svg>
                </a>
               {{--  <div class="-mr-2 flex items-center md:hidden">
                    <button type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" id="main-menu" aria-label="Main menu" aria-haspopup="true">
                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                    </button>
                </div>
 --}}            </div>
        </div>
        <div class="md:ml-10 md:pr-4">
            {{-- <a href="#" class="font-medium text-gray-500 hover:text-gray-900 transition duration-150 ease-in-out">Product</a> --}}
            <a href="{{route('login')}}" class="ml-8 font-medium text-green-600 hover:text-green-900 transition duration-150 ease-in-out">Войти</a>
        </div>
    </nav>
</div>
{{-- <div x-data="{open: true}" x-show="open" class="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden z-20">
    <div class="rounded-lg shadow-md">
        <div class="rounded-lg bg-white shadow-xs overflow-hidden" role="menu" aria-orientation="vertical" aria-labelledby="main-menu">
            <div class="px-5 pt-4 flex items-center justify-between">
                <div>
                    <img class="h-8 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-on-white.svg" alt="">
                </div>
                <div class="-mr-2">
                    <button type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" aria-label="Close menu">
                        <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>
            </div>
            <div class="px-2 pt-2 pb-3">
                <a href="#" class="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out" role="menuitem">Product</a>
                <a href="#" class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out" role="menuitem">Features</a>
                <a href="#" class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out" role="menuitem">Marketplace</a>
                <a href="#" class="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out" role="menuitem">Company</a>
            </div>
            <div>
                <a href="#" class="block w-full px-5 py-3 text-center font-medium text-green-600 bg-gray-50 hover:bg-gray-100 hover:text-green-700 focus:outline-none focus:bg-gray-100 focus:text-green-700 transition duration-150 ease-in-out" role="menuitem">
                    Log in
                </a>
            </div>
        </div>
    </div>
</div>
 --}}
