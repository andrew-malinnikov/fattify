<x-landing.elements.section class="py-16 bg-white">
    <main class="px-4 lg:px-0 text-center">
        <div class="font-semibold text-lg">© fattify {{ date('Y') }}</div>
    </main>
</x-landing.elements.section>
