@props([
    'withBgOverlay' => false
])
<section {{$attributes->merge(['class' => 'relative'])}}>
    @if ($withBgOverlay)
        <div class="
            absolute inset-0 z-0
            bg-green-200
            transform skew-y-3 origin-right
        ">
        </div>
    @endif
    <div class="max-w-screen-xl mx-auto mx-4 relative z-10">
        {{$slot}}
    </div>
</section>
