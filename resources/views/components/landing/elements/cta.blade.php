@props([
    'color' => 'green'
])

<div {{$attributes->merge(['class' => 'mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start lg:mt-24'])}}>
    <div class="rounded-md shadow">
        <a
            href="{{ route('register') }}"
            class="
                w-full flex items-center justify-center px-8 py-3
                border border-transparent
                text-base leading-6 font-medium text-white bg-{{$color}}-600 rounded-md
                hover:bg-{{$color}}-500
                focus:outline-none
                focus:border-{{$color}}-700
                focus:shadow-outline-{{$color}}
                transition duration-150 ease-in-out
                md:py-4 md:text-lg md:px-10"
        >
            {{ $slot }}
        </a>
    </div>
</div>
