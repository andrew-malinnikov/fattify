<x-landing.elements.section class="bg-white pt-16">
    <main class="flex justify-center items-center max-w-screen-md mx-auto px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
        <div class="sm:text-center lg:text-left">
            <h1 class="text-6xl font-black text-gray-900 tracking-wide">Fattify</h1>
            <h2 class="text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:leading-none">
                Помогает следить за калориями и БЖУ
            </h2>
            <p class="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
                Записывайте то, сколько вы съели, не важно, что. <strong>Бесплатно</strong>
            </p>
            <x-landing.elements.cta>
                Попробовать
            </x-landing.elements.cta>
        </div>
    </main>
</x-landing.elements.section>
