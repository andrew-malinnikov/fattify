<div x-data
    x-init="
        new Pikaday({
            field: $refs.input,
            trigger: $refs.trigger
        })
    "
    x-on:change="$dispatch('input', $event.target.value)"
>
    <div x-ref="trigger">
        {{$trigger}}
    </div>
    <input
        x-ref="input"
        type="hidden"
        {{ $attributes }}
    >
</div>
