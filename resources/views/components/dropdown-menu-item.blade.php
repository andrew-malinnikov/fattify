@props([
    'to',
])
<div>
    <a
        class="inline-block w-full px-4 py-2 text-green-200 hover:bg-green-500 hover:text-green-100"
        href="{{$to}}"
    >
        {{ $slot }}
    </a>
</div>
