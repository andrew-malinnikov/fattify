<div>
    <form wire:submit.prevent="update">
        <div class="mt-6 sm:mt-5 space-y-6">

            <x-input.group label="Photo" for="photo" :error="$errors->first('newAvatar')">
                <x-input.file-upload wire:model="newAvatar" id="new-avatar">
                    <div class="flex items-center space-x-1">
                        <img
                            src="{{ $newAvatar ? $newAvatar->temporaryUrl() : auth()->user()->avatarUrl() }}"
                            alt="Your new avatar"
                            class="w-16 h-16 rounded-full border-4 border-white shadow"
                        >
                        @if ($newAvatar)
                            <button wire:click.prevent="$set('newAvatar', null)">
                                <x-icons.trash class="w-5 h-5 stroke-current text-red-500" />
                            </button>
                        @endif
                    </div>
                </x-input.file-upload>
            </x-input.group>

            <x-input.group label="Name" for="name" :error="$errors->first('name')">
                <x-input.text wire:model="name" id="name" />
            </x-input.group>
        </div>

        <div class="mt-8 border-t border-gray-200 pt-5">
            <div class="space-x-3 flex justify-end items-center">
                <span x-data="{ open: false }" 
                    x-init="
                        @this.on('notify-saved', () => {
                            if (open === false) setTimeout(() => { open = false }, 2500);
                            open = true;
                        })
                    " 
                    x-show.transition.out.duration.1000ms="open" 
                    style="display: none;" 
                    class="text-gray-500"
                >
                    Saved!
                </span>

                <span class="inline-flex rounded-md shadow-sm">
                    <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-green-600 hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green active:bg-green-700 transition duration-150 ease-in-out">
                        Save
                    </button>
                </span>
            </div>
        </div>
    </form>
</div>
