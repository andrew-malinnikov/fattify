<div class=" pb-20 min-h-screen">

    {{-- Diary Date --}}
    <x-diary.diary-date :date="$date" />

    <!-- Hero -->
    <x-diary.daily-overview :totalDailyConsumption="$totalDailyConsumption"/>

    <!-- Breakdown -->
    <x-diary.daily-entries :dailyResults="$dailyResults"/>

    <x-modals.modal>
        <livewire:app.diary.write
            name="write"
            :date="$date"
            :meal-time="$writingToMealTime"
            :key="Illuminate\Support\Str::random(18)"
        />
    </x-modals.modal>
</div>

