<div 
    x-data="writeToDiary()" 
    x-init="
        $watch('calculatedMacros', value => {
            @this.set('fat', value.fat)
            @this.set('carbohydrates', value.carbohydrate)
            @this.set('protein', value.protein)
            @this.set('dishName', food.name)
        })
        $watch('searchKeyword', value => searched = false)
        $watch('primaryServingQuantity', value => calculateMacros())
    "
>
    <div class="px-6 py-4 bg-gray-200 rounded-tr-lg rounded-tl-lg">
        <div>
            <h3 class="font-black text-gray-700 text-2xl">So how much was it?</h3>
        </div>
        <div class="mt-2 flex items-center justify-between">
            <x-form.date-picker wire:model="date">
                <x-slot name="trigger">
                    <button class="flex items-center focus:outline-none focus:shadow-outline">
                        <x-icons.calendar class="stroke-current w-5 h-5 text-green-500" />
                        <span class="inline-block ml-2">{{$date}}</span>
                    </button>
                </x-slot>
            </x-form.date-picker>
            <span class="inline-block px-1 py-1 bg-green-200 text-green-700 font-semibold uppercase rounded text-xs tracking-wider">
                {{$mealTime}}
            </span>
        </div>
    </div>
    <div>
        <div class="flex bg-gray-200">
            <button
                x-on:click.prevent="setMode('manual')"
                class="px-6 py-3 rounded-tr-lg  flex-1 inline-block w-full text-left text-base font-bold focus:outline-none hover:bg-gray-300  transition duration-300"
                :class="mode == 'manual' ? 'bg-white': ''"
            >
                Macros
            </button>
            <button
                x-on:click.prevent="setMode('search')"
                class="px-6  py-3 rounded-tl-lg flex-1 inline-block w-full text-left text-base font-bold focus:outline-none hover:bg-gray-300  transition duration-300"
                :class="mode == 'search' ? 'bg-white': ''"
            >
                <span x-show="! food">Search Food</span>
                <div x-show="food" class="flex items-center space-x-1.5">
                    <a x-on:click.prevent="backToSearch">
                        <x-icons.x-circle class="h-5 w-5 fill-current text-green-300" />
                    </a>
                    <span x-text="food.name"></span>
                </div>
            </button>
        </div>
        <div x-show="mode == 'manual'" class="px-6 pt-3 pb-6 bg-white">
            @include ('app.diary._write-macros-form')
        </div>
        <div x-show="mode == 'search'" class="px-6 pt-3 pb-6 bg-white">
            <div x-show="! food">
                @include ('app.diary._search-food-form')
            </div>
            <div x-show="food" class="relative">
                @include ('app.diary._select-searched-food')
            </div>
        </div>
    </div>
    <div class="bg-green-300 rounded-br-lg rounded-bl-lg">
        <div class="px-6 py-4 flex justify-between items-center">
            <button
                x-on:click.prevent="mode = 'search'"
                class="mr-4 text-green-900 underline text-sm font-bold"
            >
                I don't know
            </button>
            <button
                wire:click.prevent="write"
                class="px-4 py-2 bg-green-300 border-2 border-white text-green-800 uppercase text-sm tracking-wide font-bold rounded-lg shadow-md hover:bg-green-400"
            >
                write to diary
            </button>
        </div>
    </div>
</div>

@push('scripts')
<script>
   const writeToDiary = () => {
        return {
            mode: 'manual',
            searched: false,
            searchResults: [],
            food: '',
            searchKeyword: '',
            primaryServingQuantity: '',
            loading: false,
            calculatedMacros: {
                calories: '',
                fat: '',
                protein: '',
                carbohydrate: '',
            },

            setMode(mode)  {
                this.mode = mode
            },

            findFoodById(id) {
                this.loading = true
                axios.get(route('api.food-provider.foods.show', {foodId: id}))
                    .then(response => {
                        this.food = response.data.data
                        this.loading = false
                        this.primaryServingQuantity = 100
                    })
            },
            search() {
                if (this.searchKeyword == '') return
                this.loading = true
                axios.get(route('api.food-provider.search.index', {keyword: this.searchKeyword}))
                    .then(response => {
                        this.searched = true
                        this.searchResults = response.data.data
                        this.loading = false
                    })
            },
            backToSearch() {
                this.food = ''
                this.primaryServingQuantity = ''
            },
            calculateMacros() {
                if (! this.primaryServingQuantity) return
                const serving = this.food.primaryServing
                this.calculatedMacros = {
                    calories: Math.round(serving.calories * this.primaryServingQuantity),
                    fat: Math.round(serving.fat * this.primaryServingQuantity),
                    protein: Math.round(serving.protein * this.primaryServingQuantity),
                    carbohydrate: Math.round(serving.carbohydrate * this.primaryServingQuantity),
                }
            }
        }
    }
</script>
@endpush
