<div>
  <div x-data="{expandEntries: false }">
    <div class="bg-white rounded-lg shadow overflow-hidden">
      <div class="py-6 flex justify-between items-center px-4">
        <div class="font-bold text-green-500 uppercase text-xs tracking-wider">{{ $meal }} </div>
        <div class="flex items-center">
          <span class="mr-6">{{ $totalConsumptionPerMeal->calorie }}</span>
          <button x-data x-on:click="$dispatch('open-modal'); $dispatch('input', 'Banana')">go go</button>
          {{-- @include ('app.diary._add-entry-to-meal-time') --}}
        </div>
      </div>
      @include ('app.diary._meal-time-overview')
      @include ('app.diary._meal-time-diary-entries')
    </div>

    @include ('app.diary._meal-time-diary-entries-toggle')
  </div>
</div>
