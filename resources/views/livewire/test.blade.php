<div>
    <button wire:click="showMessage">show me</button>
    <input x-data="test()" x-on:input.keydown.enter="syncData">
    <span
        x-data="{ open: true }"
        x-init="
            @this.on('notify-saved', () => {
                console.log('lol');
                if (open === false) setTimeout(() => { open = false }, 2500);
                open = true;
            })
        "
        x-show.transition.out.duration.1000ms="open"
        style="display: none;"
        class="text-gray-500"
    >Saved!</span>
</div>

@push('scripts')


<script>

    const test = () => {
        return {
            syncData(e) {
                console.log(e.target.value);
                @this.set('foo', e.target.value)
            }
        }
    }

</script>
@endpush
