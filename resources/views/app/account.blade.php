@extends ('layouts.app')

@section ('title', 'My Account')

@section ('heading', 'My Account')

@section ('content')
    <div class="flex flex-col">
        <x-account.account-tabs />
        <div class="flex-1">
            <livewire:app.account />
        </div>
    </div>
@stop
