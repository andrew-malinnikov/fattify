<div class="flex bg-green-100 py-4">
  @if ($diaryEntries->count() > 0)
    <div class="flex-1 flex flex-col items-center">
      <h4 class="text-gray-500 text-sm font-semibold">Fat</h4>
      <span class="mt-1 text-gray-600 text-xl font-bold">{{ $diaryEntries->groupByMacros()->fat }} g</span>
    </div>
    <div class="flex-1 flex flex-col items-center">
      <h4 class="text-gray-500 text-sm font-semibold">Carbohydrate</h4>
      <span class="mt-1 text-gray-600 text-xl font-bold">{{ $diaryEntries->groupByMacros()->carbohydrates }} g</span>
    </div>
    <div class="flex-1 flex flex-col items-center">
      <h4 class="text-gray-500 text-sm font-semibold">Protein</h4>
      <span class="mt-1 text-gray-600 text-xl font-bold">{{ $diaryEntries->groupByMacros()->protein }} g</span>
    </div>
  @else
    <div class="px-4 text-sm text-gray-500">Nothing for <span>{{ $mealTime }}</span> yet...</div>
  @endif
</div>
