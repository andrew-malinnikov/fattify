<div class="flex">
    <div class="flex-1">
        <div class="font-semibold text-gray-700">Fat</div>
        <div class="mt-1">
            <input wire:model="fat" class="w-full form-input" type="tel" placeholder="Fat">
        </div>
    </div>
    <div class="flex-1 ml-3">
        <div class="font-semibold text-gray-700">Carbs</div>
        <div class="mt-1">
            <input wire:model="carbohydrates" class="w-full form-input" type="tel" placeholder="Carbs">
        </div>
    </div>
    <div class="flex-1 ml-3">
        <div class="font-semibold text-gray-700">Protein</div>
        <div class="mt-1">
            <input wire:model="protein" class="w-full form-input" type="tel" placeholder="Prot">
        </div>
    </div>
</div>
<div class="mt-1">
    @error('fat')
    <p class="italic text-base text-pink-500 leading-tight">{{$message}}</p>
    @enderror
</div>
<div class="mt-1">
    @error('carbohydrates')
    <p class="italic text-base text-pink-500 leading-tight">{{$message}}</p>
    @enderror
</div>
<div class="mt-1">
    @error('protein')
    <p class="italic text-base text-pink-500 leading-tight">{{$message}}</p>
    @enderror
</div>
<div class="mt-3">
    <div class="font-semibold text-gray-700">What was it?</div>
    <div class="mt-1">
        <input 
            wire:model="dishName" 
            class="w-full form-input" 
            type="text" 
            placeholder="Chicken a-la Driken"
        >
        @error('dishName')
            <p class="mt-1 italic text-base text-pink-500 leading-tight">{{$message}}</p>
        @enderror
    </div>
</div>
