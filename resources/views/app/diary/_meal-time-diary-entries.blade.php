<div 
    x-show="expandEntries" 
    style="display: none;" 
    class="divide-y-4 divide-gray-100"
>
    @foreach ($diaryEntries as $entry)
    <div class="relative">
        <div class="border-l-4 border-green-300">
            <div>
                <div class="w-full pt-3 px-6 flex  items-center space-x-2">
                    <span class="text-gray-500 uppercase text-xs tracking-wider">{{ $entry->dish_name }}</span>
                    <span class="text-gray-500 text-sm">{{ $entry->calorie }} cal.</span>
                </div>
            </div>
            <div class="w-full flex py-3">
                <div class="flex-1 text-green-700 font-bold text-sm text-center">
                    {{$entry->fat}}
                </div>
                <div class="flex-1 text-green-700 font-bold text-sm text-center">
                    {{$entry->carbohydrates}}
                </div>
                <div class="flex-1 text-green-700 font-bold text-sm text-center">
                    {{$entry->protein}}
                </div>
            </div>
            <div class="absolute top-0 right-0 mr-4 pb-4 h-full flex items-end">
                <button wire:click.prevent="removeEntry({{$entry->id}})">
                    <x-icons.trash class="stroke-current w-4 h-4 text-orange-600" />
                </button>
            </div>
        </div>
    </div>
    @endforeach
</div>
