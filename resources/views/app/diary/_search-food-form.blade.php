<div>
    <div class="font-semibold text-gray-700">What exactly did you eat?</div>
    <div class="mt-1">
        <div class="relative">
            <div class="absolute left-0 top-0 flex items-center h-full pl-2">
                <svg
                    class="w-6 h-6 stroke-current text-gray-500"
                    :class="{'animate-spin': loading}"
                    fill="none"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="1"
                    viewBox="0 0 24 24"
                >
                    <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                </svg>
            </div>
            <input
                x-model="searchKeyword"
                x-on:keydown.enter="search()"
                class="pl-10 w-full form-input"
                type="text"
                placeholder="Banana"
            >
            <div class="absolute right-0 top-0 flex items-center h-full justify-end">
                <div class="flex h-full items-center space-x-2">
                    <button
                        x-on:click="searchKeyword = ''"
                        x-show="searchKeyword.length > 0"
                        class="text-teal-700"
                    >
                        <x-icons.x-circle class="h-5 w-5 fill-current text-orange-400" />
                    </button>
                    <button
                        x-on:click="search()"
                        class="text-xs uppercase tracking-widest font-semibold bg-gray-200 rounded-tr-md rounded-br-md border-gray-100 border-l-4 h-full px-4 transition duration-300 hover:bg-gray-300 focus:outline-none focus:shadow-outline"
                    >search</button>
                </div>
            </div>
        </div>
    </div>

    <div class="relative">
        <div class="absolute bg-white overflow-y-auto w-full z-10 max-h-64 mt-2 rounded-lg shadow-lg">
            <div x-show="searchResults.length > 0 && searched">
                <div class="divide-y-4 divide-gray-100">
                    <template x-for="item in searchResults" :key="item.food_id">
                        <div
                            x-on:click="findFoodById(item.food_id)"
                            class="px-4 py-3 group cursor-pointer hover:bg-green-500 duration-300 transition"
                        >
                            <div class="flex justify-between items-center">
                                <h4 class="group-hover:text-white font-bold text-lg" x-text="item.name"></h4>
                                <button class="text-white group-hover:text-green-100 transition duration-300">
                                    <x-icons.check class="stroke-current w-5 h-5 hover:text-white" stroke-width="4"  />
                                </button>
                            </div>
                            <div class="mt-2">
                                <p class="group-hover:text-green-100 text-gray-500" x-text="item.description"></p>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
            <div x-show="searchResults.length == 0 && searched" class="px-4 py-2 text-lg tracking-wide text-gray-700">
                <div class="flex items-center space-x-2">
                    <svg class="w-6 h-6 stroke-current text-orange-400" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="1" viewBox="0 0 24 24" stroke="currentColor"><path d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg>
                    <span>Nothing found for <span x-text="searchKeyword"></span></span>
                </div>
            </div>
        </div>
   </div>
</div>

