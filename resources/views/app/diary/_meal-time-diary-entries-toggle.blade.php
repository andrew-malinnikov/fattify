<div>
  @if ($diaryEntries->count() > 0)
    <div class="relative flex justify-center">
      <div class="absolute bottom-0 rounded-md bg-green-100 p-1 -mb-4 shadow-lg">
        <button x-on:click.prevent="expandEntries = !expandEntries" class="block">
          <svg x-show="! expandEntries" style="display: none;" class="stroke-current w-5 h-5 text-green-800" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><polyline points="6 9 12 15 18 9"></polyline></svg>
          <svg x-show="expandEntries" style="display: none;" class="stroke-current w-5 h-5 text-green-800" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><polyline points="18 15 12 9 6 15"></polyline></svg>
        </button>
      </div>
    </div>
  @endif
</div>
