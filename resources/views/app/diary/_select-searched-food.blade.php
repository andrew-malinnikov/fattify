<div>
    <template x-if="food.primaryServing">
        <div class="pt-4 flex space-x-10">
            <div class="w-1/2">
                <input
                    type="tel"
                    x-model="primaryServingQuantity"
                    class="form-input w-full"
                    placeholder="grams"
                >
                <span class="inline-block mt-2 text-gray-500">Approximate amount of grams you have had of this.</span>
            </div>

            <div class="w-1/2">
                <div class="grid grid-cols-2 gap-4">
                    <div>
                        <div class="text-gray-500 uppercase text-xs font-semibold tracking-wider">Calories</div>
                        <div class="font-bold text-lg text-green-500" x-text="calculatedMacros.calories"></div>
                    </div>
                    <div>
                        <div class="text-gray-500 uppercase text-xs font-semibold tracking-wider">Fat</div>
                        <div class="font-bold text-lg text-green-500" x-text="calculatedMacros.fat"></div>
                    </div>
                    <div>
                        <div class="text-gray-500 uppercase text-xs font-semibold tracking-wider">Carbs</div>
                        <div class="font-bold text-lg text-green-500" x-text="calculatedMacros.carbohydrate"></div>
                    </div>
                    <div>
                        <div class="text-gray-500 uppercase text-xs font-semibold tracking-wider">Protein</div>
                        <div class="font-bold text-lg text-green-500" x-text="calculatedMacros.protein"></div>
                    </div>
                </div>
            </div>


        </div>
    </template>

    <template x-if="! food.primaryServing && food.servings && food.servings.length > 0">
        <div>
            Here are some examples of servings

            <template x-for="serving in food.servings">
                <div>
                    <div>
                        <span x-text="serving.unit"></span>
                        <span x-text="serving.amount"></span>
                    </div>

                    <div>
                        <div>
                            <span>Fat:</span>
                            <span x-text="serving.fat"></span>
                        </div>
                        <div>
                            <span>Carbs:</span>
                            <span x-text="serving.carbohydrates"></span>
                        </div>
                        <div>
                            <span>Protein:</span>
                            <span x-text="serving.protein"></span>
                        </div>
                    </div>

                    <div>
                        <span>Calories:</span>
                        <span x-text="serving.calories"></span>
                    </div>
                </div>
            </template>
        </div>
    </template>
</div>
