@extends ('layouts.app')
@section ('title', 'My Diary')

@section ('content')
  @livewire('app.diary')
@stop
