<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body class="font-sans antialiased text-gray-900 bg-gray-100">
  <div id="app">
    <header class="container mx-auto h-20 flex items-center justify-between px-4 bg-white">
      <div class="text-2xl font-black">
        <a href="/">Fattify</a>
      </div>
      <div>
        <dropdown-menu
          :is-auth="{{ json_encode(auth()->check()) }}"
        ></dropdown-menu>
      </div>
    </header>
    <div class="container">
      @yield('content')
    </div>

    <portal-target name="modals"></portal-target>
  </div>
  <script src="/js/app.js"></script>
</body>
</html>
