<?php

namespace Tests\Food\FoodProviders;

use App\Food\FoodProviders\FakeFoodProvider;
use App\Food\FoodProviders\Food;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FakeFoodProviderTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_seraches_for_food()
    {
        $provider = new FakeFoodProvider();

        $results = $provider->search('banana');

        $this->assertEquals(2, $results->count());
        $this->assertInstanceOf(Food::class, $item = $results->first());
        $this->assertArrayHasKey('food_id', $item->toArray());
        $this->assertArrayHasKey('name', $item->toArray());
        $this->assertArrayHasKey('description', $item->toArray());
    }

    /**
     * @test
     * @covers \App\Food\FoodProviders\FakeFoodProvider::find()
     */
    public function it_finds_a_food_by_id()
    {
        $provider = new FakeFoodProvider();
        $foodId = random_int(10, 20);

        $food = $provider->find($foodId);

        $this->assertInstanceOf(Food::class, $food);
        $this->assertArrayHasKey('food_id', $food->toArray());
        $this->assertArrayHasKey('name', $food->toArray());
        $this->assertArrayHasKey('description', $food->toArray());
        $this->assertArrayHasKey('servings', $food->toArray());
    }
}
