<?php

namespace Tests\Food\FoodProviders;

use App\Food\FoodProviders\Serving;
use App\Food\FoodProviders\Servings;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ServingsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Servings::fromFatSecretResponse
     */
    public function it_builds_instance_from_fat_secret_response_data()
    {
        $data = [
            'calories' => '90',
            'carbohydrate' => '24.00',
            'cholesterol' => '0',
            'fat' => '0',
            'fiber' => '1.0',
            'measurement_description' => 'serving',
            'metric_serving_amount' => '100.000',
            'metric_serving_unit' => 'g',
            'number_of_units' => '1.000',
            'polyunsaturated_fat' => '0',
            'protein' => '1.00',
            'saturated_fat' => '0',
            'serving_description' => '2 mini bananas',
            'serving_id' => '990004',
            'serving_url' => 'https://www.fatsecret.com/calories-nutrition/del-monte/mini-bananas',
            'sodium' => '0',
            'sugar' => '15.00',
        ];

        $servings = Servings::fromFatSecretResponse($data);

        $this->assertCount(1, $servings);
        $this->assertEquals('1.00', $servings[0]->protein);
        $this->assertEquals('990004', $servings[0]->serving_id);
    }

    /**
     * @test
     * @covers \Servings::primary
     */
    public function it_detects_primary_serving()
    {
        $data = [
            $primaryServing = new Serving([
                'unit' => 'g',
                'description' => 'g',
                'amount' => '100.000',
                'fat' => '0.33',
                'carbohydrate' => '22.84',
                'protein' => '1.09',
                'calories' => '89',
            ]),
            new Serving(['unit' => 'g', 'amount' => '28.350', 'description' => 'oz']),
        ];
        $servings = new Servings($data);

        $fetchedPrimary = $servings->primary();

        $this->assertInstanceOf(Serving::class, $fetchedPrimary);
        $this->assertEquals('g', $fetchedPrimary->unit);
        $this->assertEquals('g', $fetchedPrimary->description);
        $this->assertEquals(1, $fetchedPrimary->amount);
        $this->assertEquals((float) '0.33' / 100, $fetchedPrimary->fat);
        $this->assertEquals((float) '22.84' / 100, $fetchedPrimary->carbohydrate);
        $this->assertEquals((float) '1.09' / 100, $fetchedPrimary->protein);
        $this->assertEquals((float) '89' / 100, $fetchedPrimary->calories);
    }

    /**
     * @test
     */
    public function it_may_not_have_primary_serving()
    {
        $data = [
            new Serving(['unit' => 'g', 'amount' => '28.350', 'description' => 'oz']),
        ];
        $servings = new Servings($data);

        $fetchedPrimary = $servings->primary();

        $this->assertNull($fetchedPrimary);
    }
}
