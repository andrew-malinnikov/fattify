<?php

use App\Food\FoodProviders\Food;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FoodTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function food_consists_of_fields()
    {
        $params = [
            'name' => 'Foo',
            'description' => 'bar description',
            'food_id' => '123456',
            'url' => 'https://example.com/food/id',
            'type' => Food::TYPE_GENERIC,
            'brand' => 'Pepsi',
            'primaryServing' => null,
            'servings' => null,
        ];

        $food = new Food($params);

        $this->assertEquals($food->toArray(), $params);
        $this->assertTrue($food->isGeneric());
    }
}
