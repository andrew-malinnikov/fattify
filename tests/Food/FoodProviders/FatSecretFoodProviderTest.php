<?php

namespace Tests\Food\FoodProviders;

use App\Food\FoodProviders\Food;
use App\Food\FoodProviders\FoodProvider;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * @group third-party-integration
 */
class FatSecretFoodProviderTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_seraches_for_food()
    {
        $provider = app(FoodProvider::class);

        $results = $provider->search('banana');

        $this->assertInstanceOf(Food::class, $item = $results->first());
        $this->assertArrayHasKey('food_id', $item->toArray());
        $this->assertArrayHasKey('name', $item->toArray());
        $this->assertArrayHasKey('description', $item->toArray());
    }

    /** @test */
    public function it_finds_a_food_by_id()
    {
        $provider = app(FoodProvider::class);

        $food = $provider->find($foodId = 35755 /**banana ID**/);
        // $food = $provider->find($foodId = 983492 /** mini banana branded**/);

        $this->assertInstanceOf(Food::class, $food);
        $this->assertNotNull($food->servings);
        $this->assertNotNull($food->primaryServing);
    }
}
