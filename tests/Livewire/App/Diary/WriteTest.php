<?php

namespace Tests\Livewire\App\Diary;

use App\NutritionDiary\Facades\Calorie;
use App\NutritionDiary\MealTime;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class WriteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Write::write
     */
    public function it_writes_down_new_diary_entry()
    {
        $user = factory(User::class)->create();
        Calorie::shouldReceive('calculate')->andReturn(100);

        Livewire::actingAs($user)->test('app.diary.write', [
            'mealTime' => MealTime::BREAKFAST,
            'date' => today()->subDays(1)->toDateString(),
        ])
            ->fill([
                'fat' => '10',
                'carbohydrates' => '20',
                'protein' => '30',
                'dishName' => 'Chicken',
            ])
            ->call('write')
            ->assertEmitted('diary-written')
            ->assertDispatchedBrowserEvent('close-modal');

        $this->assertEquals(1, $user->nutritionDiaryEntries()->count());
        $entry = $user->nutritionDiaryEntries()->first();
        $this->assertEquals(10, $entry->fat);
        $this->assertEquals(20, $entry->carbohydrates);
        $this->assertEquals(30, $entry->protein);
        $this->assertEquals(100, $entry->calorie);
        $this->assertEquals(today()->subDay()->toDateString(), $entry->date->toDateString());
        $this->assertEquals(MealTime::BREAKFAST, $entry->meal_time);
        $this->assertEquals('Chicken', $entry->dish_name);
    }

    /**
     * @test
     * @covers \Write::write
     */
    public function it_can_write_an_entry_with_just_one_macro_specified()
    {
        $user = factory(User::class)->create();
        Calorie::shouldReceive('calculate')->andReturn(100);

        Livewire::actingAs($user)->test('app.diary.write', [
            'mealTime' => MealTime::BREAKFAST,
            'date' => today()->subDays(1)->toDateString(),
        ])
            ->fill([
                'fat' => '10',
                'carbohydrates' => '',
                'protein' => '',
                'dishName' => '',
            ])
            ->call('write')
            ->assertEmitted('diary-written');

        $this->assertEquals(1, $user->nutritionDiaryEntries()->count());
        $entry = $user->nutritionDiaryEntries()->first();
        $this->assertEquals(10, $entry->fat);
        $this->assertNull($entry->carbohydrates);
        $this->assertNull($entry->protein);
        $this->assertEquals(100, $entry->calorie);
        $this->assertEquals(today()->subDay()->toDateString(), $entry->date->toDateString());
        $this->assertEquals(MealTime::BREAKFAST, $entry->meal_time);
        $this->assertNull($entry->dish_name);
    }

    /**
     * @test
     * @covers \Write::write
     */
    public function it_requires_at_least_one_macro_to_write_an_entry()
    {
        $user = factory(User::class)->create();

        Livewire::actingAs($user)->test('app.diary.write', [
            'mealTime' => MealTime::BREAKFAST,
            'date' => today()->subDays(1)->toDateString(),
        ])
            ->fill([
                'fat' => '',
                'carbohydrates' => '',
                'protein' => '',
                'dishName' => 'Chicken',
            ])
            ->call('write')
            ->assertHasErrors('fat')
            ->assertHasErrors('carbohydrates')
            ->assertHasErrors('protein');

        $this->assertEquals(0, $user->nutritionDiaryEntries()->count());
    }

    /**
     * @test
     * @covers \Write::write
     */
    public function macros_must_be_positive_numerics()
    {
        $user = factory(User::class)->create();

        Livewire::actingAs($user)->test('app.diary.write', [
            'mealTime' => MealTime::BREAKFAST,
            'date' => today()->subDays(1)->toDateString(),
        ])
            ->fill([
                'fat' => '-1',
                'carbohydrates' => 'string',
                'protein' => '33.3',
                'dishName' => 'Chicken',
            ])
            ->call('write')
            ->assertHasErrors('fat')
            ->assertHasErrors('carbohydrates');

        $this->assertEquals(0, $user->nutritionDiaryEntries()->count());
    }
}
