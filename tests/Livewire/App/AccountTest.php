<?php

namespace Tests\Livewire\App;

use App\Goals\Goal;
use App\Users\User;
use Tests\TestCase;
use Livewire\Livewire;
use Illuminate\Http\Testing\File;
use App\Http\Livewire\App\Account;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Modules\Sales\Infrastructure\PostCutoffChanges\SkipOrderPostCutoffService;

class AccountTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers Account::handle
     */
    public function it_updates_an_avatar()
    {
        Storage::fake('avatars');
        $user = factory(User::class)->create([
            'name' => 'andrew',
            'avatar' => null,
        ]);

        Livewire::actingAs($user)->test(Account::class, ['user' => $user])
            ->set('newAvatar', File::image('avatar.png'))
            ->set('name', 'New name')
            ->call('update');

        $this->assertNotNull($user->fresh()->avatar);
        $this->assertEquals('New name', $user->name);
        Storage::disk('avatars')->assertExists($user->avatar);
    }

    /**
     * @test
     */
    public function it_doesnt_update_avatar_if_no_new_one_provided()
    {
        Storage::fake('avatars');
        $user = factory(User::class)->create([
            'name' => 'andrew',
        ]);

        Livewire::actingAs($user)->test(Account::class, ['user' => $user])
            ->set('newAvatar', null)
            ->set('name', 'New name')
            ->call('update');

        $this->assertEquals('New name', $user->name);
        $this->assertNull($user->avatar);
        Storage::disk('avatars')->assertMissing($user->avatar);
    }

    /**
     * @test
     * @covers Account::mount
     */
    public function it_knows_if_user_has_a_goal()
    {
        $user = factory(User::class)->create();
        $goal = factory(Goal::class)->create([
            'user_id' => $user->id,
        ]);

        $account = Livewire::actingAs($user)->test(Account::class);
    }
}
