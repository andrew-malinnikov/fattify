<?php

namespace Tests\Livewire\App;

use App\NutritionDiary\MealTime;
use App\NutritionDiary\NutritionDiaryEntry;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class DiaryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Diary::handle
     */
    public function it_renders_livewire_component_for_logged_in_user()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)->get(route('my-diary'))
            ->assertSeeLivewire('app.diary');
    }

    /**
     * @test
     */
    public function it_reads_current_total_daily_consumption()
    {
        $date = today()->toDateString();

        $user = factory(User::class)->create();
        factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => $date,
            'fat' => 40,
            'carbohydrates' => 30,
        ]);

        Livewire::actingAs($user)->test('app.diary')
            ->set('date', $date)
            ->assertViewHas('totalDailyConsumption')
            ->assertSee(40)
            ->assertSee(30);
    }

    /**
     * @test
     */
    public function it_shows_all_meal_times()
    {
        $user = factory(User::class)->create();
        Livewire::actingAs($user)->test('app.diary')
            ->assertSee(MealTime::BREAKFAST)
            ->assertSee(MealTime::LUNCH)
            ->assertSee(MealTime::DINNER)
            ->assertSee(MealTime::OTHER);
    }

    /**
     * @test
     */
    public function it_removes_a_single_diary_entry()
    {
        $user = factory(User::class)->create();
        $entry = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
        ]);

        Livewire::actingAs($user)->test('app.diary')
            ->call('removeEntry', $entry->id);

        $this->assertEquals(0, NutritionDiaryEntry::count());
    }
}
