<?php

use App\NutritionDiary\Facades\OpenDiary;
use App\NutritionDiary\NutritionDiary;
use App\NutritionDiary\NutritionDiaryReader;
use App\NutritionDiary\NutritionDiaryWriter;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class NutritionDiaryTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_be_resolved()
    {
        $diary = OpenDiary::getFacadeRoot();

        $this->assertInstanceOf(NutritionDiary::class, $diary);
    }

    /** @test */
    public function it_can_be_resolved_for_user()
    {
        $user = factory(User::class)->create();

        $diary = OpenDiary::forUser($user);

        $this->assertInstanceOf(NutritionDiary::class, $diary);
        $this->assertTrue($diary->getUser()->is($user));
    }

    /** @test */
    public function it_can_be_opened_on_a_date()
    {
        $date = today();

        $diary = OpenDiary::on($date);

        $this->assertInstanceOf(NutritionDiary::class, $diary);
        $this->assertEquals($diary->date(), $date);
    }

    /** @test */
    public function it_resolves_diary_writer()
    {
        $date = today();
        $user = factory(User::class)->create();
        $diary = OpenDiary::on($date)->forUser($user);

        $writer = $diary->write();

        $this->assertInstanceOf(NutritionDiaryWriter::class, $writer);
    }

    /** @test */
    public function it_resolves_diary_reader()
    {
        $reader = OpenDiary::read();

        $this->assertInstanceOf(NutritionDiaryReader::class, $reader);
    }
}
