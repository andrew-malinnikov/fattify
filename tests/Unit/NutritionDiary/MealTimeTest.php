<?php

use App\NutritionDiary\MealTime;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class MealTimeTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_lists_meal_times_to_assoc()
    {
        $this->assertArrayHasKey(MealTime::BREAKFAST, MealTime::listToAssoc());
    }

    /** @test */
    public function it_gets_all_meal_times()
    {
        $this->assertContains(
            MealTime::BREAKFAST,
            MealTime::getAll()
        );
    }
}
