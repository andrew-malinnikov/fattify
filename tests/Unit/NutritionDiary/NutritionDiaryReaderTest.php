<?php

namespace Tests\Unit\NutritionDiary;

use App\NutritionDiary\Facades\OpenDiary;
use App\NutritionDiary\MacrosConsumption;
use App\NutritionDiary\MealTime;
use App\NutritionDiary\NutritionDiary;
use App\NutritionDiary\NutritionDiaryEntry;
use App\NutritionDiary\NutritionDiaryEntryCollection;
use App\NutritionDiary\NutritionDiaryReader;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class NutritionDiaryReaderTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_fetches_all_diary_records()
    {
        $user = factory(User::class)->create();
        $diary = new NutritionDiary($user);
        $entry1 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today(),
        ]);
        $entry2 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today()->subDays(2),
        ]);
        $entry3 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today()->subDays(2),
        ]);

        $records = (new NutritionDiaryReader($diary))->get();

        $this->assertInstanceOf(NutritionDiaryEntryCollection::class, $records);
        $this->assertEquals(3, $records->count());
    }

    /** @test */
    public function it_reads_diary_entries_for_given_date()
    {
        $user = factory(User::class)->create();
        $today = today();
        $entry1 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => $today,
        ]);
        $entry2 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today()->subDays(2),
        ]);
        $entry3 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today()->subDays(2),
        ]);

        $diary = (new NutritionDiary($user))->on($today);
        $records = (new NutritionDiaryReader($diary))->get();

        $this->assertInstanceOf(NutritionDiaryEntryCollection::class, $records);
        $this->assertEquals(1, $records->count());
        $this->assertEquals($entry1->id, $records->shift()->id);
    }

    /** @test */
    public function it_reads_results_for_meal_time_only()
    {
        $user = factory(User::class)->create();
        $today = today();
        $entry1 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => $today,
            'meal_time' => MealTime::DINNER,
        ]);
        $entry2 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today()->subDays(2),
            'meal_time' => MealTime::BREAKFAST,
        ]);
        $entry3 = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'date' => today()->subDays(2),
            'meal_time' => MealTime::BREAKFAST,
        ]);

        $diary = (new NutritionDiary($user));
        $records = (new NutritionDiaryReader($diary))->setMealTime(MealTime::BREAKFAST)->get();

        $this->assertInstanceOf(NutritionDiaryEntryCollection::class, $records);
        $this->assertEquals(2, $records->count());
        $this->assertEquals($entry2->id, $records->shift()->id);
        $this->assertEquals($entry3->id, $records->shift()->id);
    }

    /** @test */
    public function it_has_a_diary_to_read_from()
    {
        $diary = OpenDiary::getFacadeRoot();

        $reader = $diary->read();

        $this->assertInstanceOf(NutritionDiary::class, $reader->getDiary());
    }

    /** @test */
    public function it_can_be_configured_to_fetch_specific_meal_time_results()
    {
        $reader = OpenDiary::read();

        $accessor = $reader->setMealTime(MealTime::LUNCH);

        $this->assertEquals(MealTime::LUNCH, $reader->getMealTime());
        $this->assertInstanceOf(NutritionDiaryReader::class, $accessor);
    }

    /**
     * @test
     */
    public function it_reads_nutrition_overview_from_diary()
    {
        $user = factory(User::class)->create();
        $dinnerA = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'fat' => 10,
            'carbohydrates' => 20,
            'protein' => 30,
        ]);

        $breakfast = factory(NutritionDiaryEntry::class)->create([
            'user_id' => $user->id,
            'fat' => 10,
            'carbohydrates' => 20,
            'protein' => 30,
        ]);
        $diary = (new NutritionDiary($user));

        $consumption = (new NutritionDiaryReader($diary))->total();

        $this->assertInstanceOf(MacrosConsumption::class, $consumption);
        $this->assertEquals(20, $consumption->fat);
        $this->assertEquals(40, $consumption->carbohydrates);
        $this->assertEquals(60, $consumption->protein);
        $this->assertEquals(200, $consumption->calorie);
    }
}
