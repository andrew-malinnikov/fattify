<?php

use App\NutritionDiary\Facades\Calorie;
use App\NutritionDiary\Facades\OpenDiary;
use App\NutritionDiary\MacrosConsumption;
use App\NutritionDiary\MealTime;
use App\NutritionDiary\NutritionDiaryEntryDetails;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class NutritionDiaryWriterTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_write_a_diary_entry()
    {
        $user = factory(User::class)->create();
        $date = today();
        $writer = OpenDiary::forUser($user)->on($date)->write();
        Calorie::shouldReceive('calculate')->andReturn(310);
        $consumption = new MacrosConsumption([
            'fat' => 100,
            'carbohydrates' => 200,
            'protein' => 300,
        ]);
        $details = new NutritionDiaryEntryDetails([
            'mealTime' => MealTime::DINNER,
            'dishName' => 'Carrot',
        ]);

        $entry = $writer->entry($details, $consumption);

        $this->assertEquals(1, $user->nutritionDiaryEntries()->count());
        $this->assertEquals(100, $entry->fat);
        $this->assertEquals(200, $entry->carbohydrates);
        $this->assertEquals(300, $entry->protein);
        $this->assertEquals($user->id, $entry->user_id);
        $this->assertEquals($date, $entry->date);
        $this->assertEquals(MealTime::DINNER, $entry->meal_time);
        $this->assertEquals('Carrot', $entry->dish_name);
        $this->assertEquals(310, $entry->calorie);
    }
}
