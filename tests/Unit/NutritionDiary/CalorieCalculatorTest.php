<?php

use App\NutritionDiary\CalorieCalculator;
use App\NutritionDiary\MacrosConsumption;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CalorieCalculatorTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_calculates_calories_from_macros()
    {
        $macros = new MacrosConsumption([
            'fat' => 100,
            'carbohydrates' => 200,
            'protein' => 300,
        ]);

        $calories = (new CalorieCalculator())->calculate($macros);

        $this->assertEquals((100 * 9) + (200 * 4) + (300 * 4), $calories);
    }

    /** @test */
    public function it_calculates_calories_from_one_macro()
    {
        $macros = new MacrosConsumption([
            'fat' => 100,
        ]);

        $calories = (new CalorieCalculator())->calculate($macros);

        $this->assertEquals((100 * 9), $calories);
    }

    /** @test */
    public function it_converts_results_to_integer()
    {
        $macros = new MacrosConsumption([
            'fat' => 100.5,
        ]);

        $calories = (new CalorieCalculator())->calculate($macros);

        $this->assertEquals((905), $calories);
    }
}
