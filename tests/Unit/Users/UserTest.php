<?php

namespace Tests\Unit\Users;

use App\Goals\Goal;
use App\Users\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Modules\Sales\Infrastructure\PostCutoffChanges\SkipOrderPostCutoffService;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers User::handle
     */
    public function it_generates_an_avatar_url()
    {
        $user = factory(User::class)->create([
            'avatar' => null,
            'email' => 'jack@email.com',
        ]);

        $url = $user->avatarUrl();

        $this->assertEquals(
            "https://www.gravatar.com/avatar/" . md5('jack@email.com'),
            $url
        );
    }

    /**
     * @test
     * @covers User::goal
     */
    public function it_might_have_a_goal()
    {
        $user = factory(User::class)->create();
        $goal = factory(Goal::class)->create(['user_id' => $user->id]);

        $this->assertTrue($user->goal->is($goal));
    }
}
