<?php

namespace Tests\Feature\Http\Controllers\Api\FoodProviders;

use App\Food\FoodProviders\FakeFoodProvider;
use App\Food\FoodProviders\FoodProvider;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class SearchControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $foodProvider = new FakeFoodProvider();
        $this->app->instance(FoodProvider::class, $foodProvider);
    }

    /** @test */
    public function it_searches_for_food_on_food_service_provider()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->json('get', route('api.food-provider.search.index'), [
            'keyword' => 'salmon',
        ]);

        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                ['food_id', 'name', 'description'],
                ['food_id', 'name', 'description'],
            ]
        ]);
    }
}
