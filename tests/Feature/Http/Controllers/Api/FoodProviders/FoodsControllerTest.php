<?php

use App\Food\FoodProviders\FakeFoodProvider;
use App\Food\FoodProviders\FoodProvider;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FoodsControllerTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_fetches_food_details()
    {
        $this->withoutExceptionHandling();
        $provider = new FakeFoodProvider();
        $this->instance(FoodProvider::class, $provider);
        $user = factory(User::class)->create();
        $foodId = random_int(10, 20);

        $response = $this->actingAs($user)->json(
            'get',
            route('api.food-provider.foods.show', ['foodId' => 35755])
        );

        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [
                'name', 'description', 'servings',
            ]
        ]);
    }
}
