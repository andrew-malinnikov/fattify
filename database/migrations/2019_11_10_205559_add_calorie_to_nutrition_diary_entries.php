<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCalorieToNutritionDiaryEntries extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('nutrition_diary_entries', function (Blueprint $table) {
            $table->integer('calorie')->after('protein')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('nutrition_diary_entries', function (Blueprint $table) {
            $table->dropColumn('calorie');
        });
    }
}
