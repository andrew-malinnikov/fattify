<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNutritionDiaryEntries extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('nutrition_diary_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->date('date');
            $table->string('meal_time');
            $table->float('fat')->nullable();
            $table->float('carbohydrates')->nullable();
            $table->float('protein')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('nutrition_diary_entries');
    }
}
