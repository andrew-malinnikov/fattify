<?php

use App\Model;
use App\Goals\Goal;
use App\Users\User;
use Faker\Generator as Faker;

$factory->define(Goal::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'type' => 'weight-loss',
        'starting_weight' => 90,
        'aim_weight' => 70,
    ];
});
