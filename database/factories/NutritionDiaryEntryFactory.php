<?php

use App\NutritionDiary\MealTime;
use App\NutritionDiary\NutritionDiaryEntry;
use App\Users\User;
use Faker\Generator as Faker;

$factory->define(NutritionDiaryEntry::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'meal_time' => MealTime::BREAKFAST,
        'date' => $faker->date,
        'fat' => 10,
        'carbohydrates' => 20,
        'protein' => 30,
        'calorie' => 100
    ];
});
