const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', 'Inter var', ...defaultTheme.fontFamily.sans],
            },
            rotate: {
              '2': '2deg',
            }
        },
        customForms: theme => ({
          default: {
            'input, select, textarea': {
              height: theme('spacing.12'),
              borderRadius: theme('borderRadius.md'),
              backgroundColor: theme('colors.gray.200'),
              padding: `${theme('spacing.0')} ${theme('spacing.3')}`,
              color: theme('colors.gray.700'),
              fontSize: theme('fontSize.xl'),
              fontWeight: theme('fontWeight.black'),
              transitionProperty: theme('transition.default'),
              '&:focus': {
                backgroundColor: theme('colors.white'),
              }
              // borderColor: theme('colors.gray.200'),
              // boxShadow: theme('boxShadow.sm'),
            },
            textarea: {
              fontSize: theme('fontSize.sm'),
              padding: `${theme('spacing.2')} ${theme('spacing.3')}`,
            },
            checkbox: {
              color: theme('colors.teal.500'),
              border: theme('borders.default'),
              borderColor: theme('colors.gray.400'),
              backgroundColor: theme('colors.gray.200'),
              width: theme('spacing.5'),
              height: theme('spacing.5'),
            },
            radio: {
              color: theme('colors.teal.500'),
              border: theme('borders.default'),
              borderColor: theme('colors.gray.400'),
              backgroundColor: theme('colors.gray.200'),
              width: theme('spacing.5'),
              height: theme('spacing.5'),
            },
          },
      }),

    },
    variants: {},
    purge: {
        content: [
            './app/**/*.php',
            './resources/**/*.html',
            './resources/**/*.js',
            './resources/**/*.jsx',
            './resources/**/*.ts',
            './resources/**/*.tsx',
            './resources/**/*.php',
            './resources/**/*.vue',
            './resources/**/*.twig',
        ],
        options: {
            defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
            whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
        },
    },
    plugins: [
        require('@tailwindcss/custom-forms'),
        require('@tailwindcss/ui'),
        require('@tailwindcss/typography'),
    ],
};
