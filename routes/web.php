<?php

Route::view('/', 'welcome')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::view('/diary', 'app.diary')->name('my-diary');
    Route::view('account', 'app.account')->name('account');
    Route::view('goal', 'app.goal')->name('goal');
});

Route::view('test', 'test');
