<?php

Route::group(['middleware' => 'auth'], function () {
    // Food Provider Search
    Route::get('food-provider/search', 'FoodProviders\SearchController@index')
        ->name('api.food-provider.search.index');

    Route::get('food-provider/foods/{foodId}', 'FoodProviders\FoodsController@show')
        ->name('api.food-provider.foods.show');
});
