<?php

namespace App\Transformers;

use App\Food\FoodProviders\Food;
use League\Fractal\TransformerAbstract;

class FoodTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param App\Food\FoodProviders\Food $item
     *
     * @return array
     */
    public function transform(Food $food)
    {
        return [
            'food_id' => $food->food_id,
            'name' => $food->name,
            'description' => $food->description,
            'url' => $food->url,
            'is_generic' => $food->isGeneric(),
            'brand' => $food->brand,
            'servings' => optional($food->servings)->toArray(),
            'primaryServing' => optional($food->primaryServing)->toArray(),
        ];
    }
}
