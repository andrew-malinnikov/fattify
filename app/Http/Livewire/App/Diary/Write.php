<?php

namespace App\Http\Livewire\App\Diary;

use App\NutritionDiary\Facades\OpenDiary;
use App\NutritionDiary\MacrosConsumption;
use App\NutritionDiary\NutritionDiaryEntryDetails;
use Carbon\Carbon;
use Livewire\Component;

class Write extends Component
{
    public $fat;
    public $carbohydrates;
    public $protein;
    public $dishName;

    public $mealTime;
    public $date;

    public function mount($mealTime, $date)
    {
        $this->mealTime = $mealTime;
        $this->date = $date;
    }

    public function write()
    {
        $this->validate([
            'fat' => ['nullable', 'required_without_all:carbohydrates,protein', 'numeric', 'min:0'],
            'carbohydrates' => ['nullable', 'required_without_all:fat,protein', 'numeric', 'min:0'],
            'protein' => ['nullable', 'required_without_all:fat,carbohydrates', 'numeric', 'min:0'],
            'dishName' => ['nullable', 'string', 'max:250'],
        ], ['fat.required_without_all' => 'Please provide at least one macro.']);

        OpenDiary::forUser(auth()->user())
            ->on(Carbon::parse($this->date))
            ->write()
            ->entry(
                new NutritionDiaryEntryDetails([
                    'mealTime' => $this->mealTime,
                    'dishName' => $this->dishName ?: null,
                ]),
                $this->buildMacrosConsumptionObject()
            );

        $this->dispatchBrowserEvent('close-modal');
        $this->emit('diary-written');

        $this->clearFields();
    }

    public function render()
    {
        return view('livewire.app.diary.write');
    }

    public function clearFields()
    {
        $this->fat = null;
        $this->carbohydrates = null;
        $this->protein = null;
        $this->dishName = null;
    }

    protected function buildMacrosConsumptionObject()
    {
        return new MacrosConsumption([
            'fat' => $this->fat ? $this->fat : null,
            'carbohydrates' => $this->carbohydrates ? $this->carbohydrates : null,
            'protein' => $this->protein ? $this->protein : null,
        ]);
    }
}
