<?php

namespace App\Http\Livewire\App;

use App\Users\User;
use Livewire\Component;
use Livewire\WithFileUploads;

class Account extends Component
{
    use WithFileUploads;

    public $name;
    public $newAvatar;
    public $goal;

    public function mount()
    {
        $this->name = auth()->user()->name;
        $this->goal = auth()->user()->goal;
    }

    public function update()
    {
        $this->validate([
            'name' => ['required'],
            'newAvatar' => ['nullable', 'image', 'max:2000'],
        ]);
        auth()->user()->update([
            'name' => $this->name,
            'avatar' => $this->newAvatar ? $this->newAvatar->store('/', 'avatars') : auth()->user()->avatar,
        ]);
        $this->emitSelf('notify-saved');
    }

    public function render()
    {
        return view('livewire.app.account');
    }
}
