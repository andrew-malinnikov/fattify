<?php

namespace App\Http\Livewire\App;

use App\NutritionDiary\Facades\OpenDiary;
use App\NutritionDiary\NutritionDiaryEntry;
use Carbon\Carbon;
use Livewire\Component;

class Diary extends Component
{
    public $date;
    public $writingToMealTime = '';

    public $listeners = ['diary-written' => '$refresh'];
    protected $updatesQueryString = ['date'];

    public function mount()
    {
        $date = request('date') ? Carbon::parse(request('date')) : today();
        $this->date = $date->toDateString();
    }

    public function updatedDate($value)
    {
        $this->date = Carbon::parse($value)->toDateString();
    }

    public function getFormattedDateProperty()
    {
        return Carbon::parse($this->date)->format('F d, Y');
    }

    public function showWriteToDiaryModal($mealTime)
    {
        $this->writingToMealTime = $mealTime;
        session()->flash('show-modal', 'write');
    }

    public function removeEntry($id)
    {
        NutritionDiaryEntry::findOrFail($id)->delete();
    }

    public function render()
    {
        return view('livewire.app.diary', [
            'totalDailyConsumption' => $this->readCurrentConsumption(),
            'dailyResults' => $this->getDailyResults(),
        ]);
    }

    protected function getDailyResults()
    {
        return OpenDiary::forUser(auth()->user())
            ->on(Carbon::parse($this->date))
            ->read()
            ->get()
            ->groupedByMealTimes();
    }

    protected function readCurrentConsumption()
    {
        return OpenDiary::forUser(auth()->user())
            ->on(Carbon::parse($this->date))
            ->read()
            ->total();
    }
}
