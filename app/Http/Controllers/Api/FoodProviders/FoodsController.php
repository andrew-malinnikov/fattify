<?php

namespace App\Http\Controllers\Api\FoodProviders;

use App\Food\FoodProviders\FoodProvider;
use App\Transformers\FoodTransformer;
use Illuminate\Http\JsonResponse;

class FoodsController
{
    protected $foods;

    public function __construct(FoodProvider $foods)
    {
        $this->foods = $foods;
    }

    /**
     * Show the food details.
     *
     * @param int $foodId
     *
     * @return JsonResponse
     */
    public function show($foodId)
    {
        $food = $this->foods->find($foodId);

        return fractal($food, FoodTransformer::class);
    }
}
