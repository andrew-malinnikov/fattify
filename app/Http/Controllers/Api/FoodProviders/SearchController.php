<?php

namespace App\Http\Controllers\Api\FoodProviders;

use App\Food\FoodProviders\FoodProvider;
use App\Transformers\FoodTransformer;

class SearchController
{
    protected $foods;

    /**
     * Instantiate the controller.
     *
     * @param App\Food\FoodProviders\FoodProvider $foods
     */
    public function __construct(FoodProvider $foods)
    {
        $this->foods = $foods;
    }

    /**
     * Perform the search on the food resource.
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $food = $this->foods->search(request('keyword'));

        return fractal($food, FoodTransformer::class);
    }
}
