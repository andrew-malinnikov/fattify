<?php

namespace App\Http\View\Composers;

use App\NutritionDiary\MealTime;
use Illuminate\View\View;

class Dictionaries
{
    /**
     * Bind data to the views.
     *
     * @param Illuminate\View\View $view
     */
    public function compose(View $view)
    {
        $view->with('mealTimes', MealTime::listToAssoc());
    }
}
