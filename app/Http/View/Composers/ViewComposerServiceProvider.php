<?php

namespace App\Http\View\Composers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        View::composer(
                ['my-diary'],
                'App\Http\View\Composers\Dictionaries'
            );
    }
}
