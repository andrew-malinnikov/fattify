<?php

namespace App\NutritionDiary;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class NutritionDiaryEntry extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dates = [
        'date',
    ];

    /**
     * Create a new Eloquent Collection instance.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new NutritionDiaryEntryCollection($models);
    }

    /**
     * Query constraint.
     *
     * @param  Illuminate\Database\Eloquent\Builder
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeOn($query, $date)
    {
        return $query->whereDate('date', '=', $date);
    }

    /**
     * Query constraint.
     *
     * @param  Illuminate\Database\Eloquent\Builder
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeFor($query, $mealTime)
    {
        return $query->whereMealTime($mealTime);
    }
}
