<?php

namespace App\NutritionDiary\Facades;

use App\NutritionDiary\CalorieCalculator;
use Illuminate\Support\Facades\Facade;

class Calorie extends Facade
{
    protected static function getFacadeAccessor()
    {
        return CalorieCalculator::class;
    }
}
