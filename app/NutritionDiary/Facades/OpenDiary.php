<?php

namespace App\NutritionDiary\Facades;

use App\NutritionDiary\NutritionDiary;
use Illuminate\Support\Facades\Facade;

class OpenDiary extends Facade
{
    protected static function getFacadeAccessor()
    {
        return NutritionDiary::class;
    }
}
