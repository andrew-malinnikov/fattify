<?php

namespace App\NutritionDiary;

use Illuminate\Database\Eloquent\Builder;

class NutritionDiaryReader
{
    /**
     * The Diary.
     *
     * @var App\NutritionDiary\NutritionDiary
     */
    protected $diary;

    /**
     * Meal Time.
     *
     * @var string
     */
    protected $mealTime;

    public function __construct(NutritionDiary $diary)
    {
        $this->diary = $diary;
    }

    /**
     * Read the Diary.
     *
     * @return NutritionDiaryEntryCollection
     */
    public function get()
    {
        return $this->buildQuery()->get();
    }

    public function total()
    {
        return $this->get()->groupByMacros();
    }

    /**
     * Build query to retrieve desired diary entries.
     *
     * @return Builder
     */
    protected function buildQuery()
    {
        $query = $this->diary
            ->getUser()
            ->nutritionDiaryEntries();

        if ($this->diary->date()) {
            $query->on($this->diary->date());
        }

        if ($this->getMealTime()) {
            $query->for($this->getMealTime());
        }

        return $query;
    }

    /**
     * Set meal time.
     *
     * @param string $mealTime
     *
     * @return self
     */
    public function setMealTime($mealTime)
    {
        $this->mealTime = $mealTime;

        return $this;
    }

    /**
     * Get the Diary.
     *
     * @return App\NutritionDiary\NutritionDiary
     */
    public function getDiary()
    {
        return $this->diary;
    }

    /**
     * Get the meal time.
     *
     * @return string
     */
    public function getMealTime()
    {
        return $this->mealTime;
    }
}
