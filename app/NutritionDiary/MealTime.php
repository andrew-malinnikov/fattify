<?php

namespace App\NutritionDiary;

class MealTime
{
    const BREAKFAST = 'breakfast';
    const LUNCH = 'lunch';
    const DINNER = 'dinner';
    const OTHER = 'other';

    /**
     * @return array
     */
    public static function listToAssoc()
    {
        return [
           static::BREAKFAST => 'Breakfast',
           static::LUNCH => 'Lunch',
           static::DINNER => 'Dinner',
           static::OTHER => 'Other',
        ];
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return array_values(array_flip(static::listToAssoc()));
    }
}
