<?php

namespace App\NutritionDiary;

use Illuminate\Support\Arr;

class CalorieCalculator
{
    protected $multipliers = [
        'fat' => 9,
        'carbohydrates' => 4,
        'protein' => 4,
    ];

    /**
     * Calculate calories off of given set of macro.
     *
     * @param array $macros
     *
     * @return int
     */
    public function calculate(MacrosConsumption $macros)
    {
        $callories = collect($macros->toArray())->map(function ($value, $macro) {
            return $value * Arr::get($this->multipliers, $macro, 0);
        })->sum();

        return (int) ceil($callories);
    }
}
