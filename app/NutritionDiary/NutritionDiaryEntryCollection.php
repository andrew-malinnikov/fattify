<?php

namespace App\NutritionDiary;

use Illuminate\Database\Eloquent\Collection;

class NutritionDiaryEntryCollection extends Collection
{
    public function groupedByMealTimes()
    {
        return $this->groupBy('meal_time')
            ->padMealTimes()
            ->sortByDesc(function ($item, $mealTime) {
                return [
                    'breakfast' == $mealTime,
                    'lunch' == $mealTime,
                    'dinner' == $mealTime,
                    'other' == $mealTime
                 ];
            });
    }

    protected function padMealTimes()
    {
        foreach (MealTime::getAll() as $mealTime) {
            if (!in_array($mealTime, array_keys($this->all()))) {
                $this[$mealTime] = new self();
            }
        }

        return $this;
    }

    /**
     * Reduce diary entries down to macros overview.
     *
     * @return $this
     */
    public function groupByMacros()
    {
        $groupped = $this->reduce(
            function ($reduced, $diaryEntry) {
                return $result = $reduced->map(function ($value, $key) use ($diaryEntry) {
                    return $value += $diaryEntry[$key];
                });
            },
            collect([
                'fat' => 0,
                'carbohydrates' => 0,
                'protein' => 0,
                'calorie' => 0,
            ])
        );

        return new MacrosConsumption($groupped->all());
    }
}
