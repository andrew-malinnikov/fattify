<?php

namespace App\NutritionDiary;

use App\NutritionDiary\Facades\Calorie;

class NutritionDiaryWriter
{
    protected $diary;

    public function __construct(NutritionDiary $diary)
    {
        $this->diary = $diary;
    }

    /**
     * Write a diary entry.
     *
     * @return App\NutritionDiary\NutritionDiaryEntry
     */
    public function entry(NutritionDiaryEntryDetails $data, MacrosConsumption $macros)
    {
        return NutritionDiaryEntry::create([
            'user_id' => $this->diary->getUser()->id,
            'date' => $this->diary->date(),
            'meal_time' => $data->mealTime,
            'fat' => $macros->fat,
            'carbohydrates' => $macros->carbohydrates,
            'protein' => $macros->protein,
            'calorie' => Calorie::calculate($macros),
            'dish_name' => $data->dishName,
        ]);
    }
}
