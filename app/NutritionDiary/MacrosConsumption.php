<?php

namespace App\NutritionDiary;

use Spatie\DataTransferObject\DataTransferObject;

class MacrosConsumption extends DataTransferObject
{
    public $fat;
    public $carbohydrates;
    public $protein;
    public $calorie;
}
