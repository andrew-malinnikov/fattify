<?php

namespace App\NutritionDiary;

use Spatie\DataTransferObject\DataTransferObject;

class NutritionDiaryEntryDetails extends DataTransferObject
{
    public string $mealTime;
    public ?string $dishName;
}
