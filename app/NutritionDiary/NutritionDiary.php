<?php

namespace App\NutritionDiary;

use Carbon\Carbon;

class NutritionDiary
{
    protected $user;
    protected $date;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    /**
     * Set User.
     *
     * @param User $user
     *
     * @return NutritionDiary
     */
    public function forUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get diary user.
     *
     * @return App\Users\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Open diary on the given date.
     *
     * @param DateTime $date
     *
     * @return NutritionDiary
     */
    public function on(Carbon $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the date the diary is open on.
     *
     * @return Carbon\Carbon
     */
    public function date()
    {
        return $this->date;
    }

    /**
     * Resolve the Writer.
     *
     * @return App\NutrtionDiary\NutritionDiaryWriter
     */
    public function write()
    {
        return resolve(NutritionDiaryWriter::class, ['diary' => $this]);
    }

    /**
     * Resolve the Reader.
     *
     * @return App\NutrtionDiary\NutritionDiaryReader
     */
    public function read()
    {
        return resolve(NutritionDiaryReader::class, ['diary' => $this]);
    }
}
