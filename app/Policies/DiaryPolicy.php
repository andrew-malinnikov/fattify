<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class DiaryPolicy
{
    use HandlesAuthorization;

    public function update($user, $actor)
    {
        return $user->id == $actor->id;
    }
}
