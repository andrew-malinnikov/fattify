<?php

namespace App\Food\FoodProviders;

use Spatie\DataTransferObject\DataTransferObjectCollection;

class Servings extends DataTransferObjectCollection
{
    public static function fromFatSecretResponse($servings)
    {
        if (is_array($servings) && count($servings) > 0 && !is_array(array_values($servings)[0])) {
            $serving = dot($servings);

            $servings = collect([new Serving([
                'serving_id' => $serving->get('serving_id'),
                'calories' => $serving->get('calories'),
                'carbohydrate' => $serving->get('carbohydrate'),
                'fat' => $serving->get('fat'),
                'protein' => $serving->get('protein'),
                'unit' => $serving->get('metric_serving_unit'),
                'amount' => $serving->get('metric_serving_amount'),
                'description' => $serving->get('measurement_description'),
            ])]);
        } else {
            $servings = collect($servings)->map(function ($serving) {
                $serving = dot($serving);

                return new Serving([
                    'serving_id' => $serving->get('serving_id'),
                    'calories' => $serving->get('calories'),
                    'carbohydrate' => $serving->get('carbohydrate'),
                    'fat' => $serving->get('fat'),
                    'protein' => $serving->get('protein'),
                    'unit' => $serving->get('metric_serving_unit'),
                    'amount' => $serving->get('metric_serving_amount'),
                    'description' => $serving->get('measurement_description'),
                ]);
            });
        }

        return new static($servings->values()->all());
    }

    public function primary()
    {
        $foundPrimaryServing = collect($this->items())
            ->first(function ($item) {
                return 'g' === $item->unit && 100 == (int) $item->amount;
            });

        if (!$foundPrimaryServing) {
            return;
        }

        return new Serving(array_merge(
            $foundPrimaryServing->toArray(),
            [
                'fat' => (float) ($foundPrimaryServing->fat) / 100,
                'carbohydrate' => (float) ($foundPrimaryServing->carbohydrate) / 100,
                'protein' => (float) ($foundPrimaryServing->protein) / 100,
                'calories' => (float) ($foundPrimaryServing->calories) / 100,
                'amount' => 1,
            ]
        ));
    }
}
