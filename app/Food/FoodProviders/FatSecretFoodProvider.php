<?php

namespace App\Food\FoodProviders;

class FatSecretFoodProvider implements FoodProvider
{
    protected $apiSecret;

    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * Search by keyword on 3rd party food service provider.
     *
     * @param string $keyword
     *
     * @return
     */
    public function search($keyword)
    {
        $response = $this->client->search($keyword);

        $response = dot($response->json());

        return collect($response->get('foods.food'))
            ->map(function ($item) {
                return Food::fromFatSecretResponse($item);
            });
    }

    /**
     * Find Food on Food Provider with all the details.
     *
     * @param int $foodId
     *
     * @return App\Food\FoodProviders\Food
     */
    public function find($foodId)
    {
        $response = $this->client->find($foodId);

        return Food::fromFatSecretResponse($response->json()['food']);
    }
}
