<?php

namespace App\Food\FoodProviders;

use Illuminate\Support\Str;

class FakeFoodProvider implements FoodProvider
{
    /**
     * Search by keyword on 3rd party food service provider.
     *
     * @param string $keyword
     *
     * @return Illuminate\Support\Collection
     */
    public function search($keyword)
    {
        return collect([$this->makeDummyFood(), $this->makeDummyFood()]);
    }

    /**
     * Find Food on Food Provider with all the details.
     *
     * @param int $foodId
     *
     * @return App\Food\FoodProviders\Food
     */
    public function find($foodId)
    {
        return $this->makeDummyFood();
    }

    /**
     * Scaffold a dummy Food instance.
     *
     * @return App\Food\FoodProviders\Food
     */
    protected function makeDummyFood()
    {
        return new Food([
            'food_id' => random_int(1, 55555),
            'name' => Str::random(8),
            'description' => Str::random(18)
        ]);
    }
}
