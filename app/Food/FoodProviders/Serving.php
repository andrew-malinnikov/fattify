<?php

namespace App\Food\FoodProviders;

use Spatie\DataTransferObject\DataTransferObject;

class Serving extends DataTransferObject
{
    public $serving_id;
    public $calories;
    public $carbohydrate;
    public $fat;
    public $protein;
    public $unit;
    public $amount;
    public $description;
}
