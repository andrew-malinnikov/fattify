<?php

namespace App\Food\FoodProviders;

use Spatie\DataTransferObject\DataTransferObject;

class Food extends DataTransferObject
{
    const TYPE_GENERIC = 'Generic';
    const TYPE_BRAND = 'Brand';

    public $food_id;
    public $name;
    public $description;
    public $url;
    public $type;
    public $brand;

    public ?\App\Food\FoodProviders\Serving $primaryServing = null;
    public ?\App\Food\FoodProviders\Servings $servings = null;

    /**
     * Check if the food is Generic.
     *
     * @return bool
     */
    public function isGeneric()
    {
        return self::TYPE_GENERIC === $this->type;
    }

    /**
     * Get facade accessor.
     *
     * @return mixed
     */
    public static function fromFatSecretResponse($data)
    {
        $item = dot($data);
        $servings = Servings::fromFatSecretResponse($item->get('servings')['serving'] ?? []);

        return new self([
            'food_id' => $item->get('food_id'),
            'name' => $item->get('food_name'),
            'description' => $item->get('food_description'),
            'type' => $item->get('food_type'),
            'url' => $item->get('food_url'),
            'brand' => $item->get('brand_name'),
            'primaryServing' => $servings->primary(),
            'servings' => $servings,
        ]);
    }
}
