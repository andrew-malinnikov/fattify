<?php

namespace App\Food\FoodProviders;

use Zttp\Zttp;

class FatSecretHttpClient
{
    /**
     * Api Key.
     *
     * @var string
     */
    protected $apiKey;

    /**
     * API Secret.
     *
     * @var string
     */
    protected $apiSecret;

    /**
     * Access Token.
     *
     * @var string
     */
    protected $accessToken;

    /**
     * Base URL.
     *
     * @var string
     */
    protected $baseUrl = 'https://platform.fatsecret.com/rest/server.api';

    /**
     * Instantiate the Client.
     *
     * @param string $apiKey
     * @param string $apiSecret
     */
    public function __construct($apiKey, $apiSecret)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    /**
     * Static constructor.
     *
     * @param string $apiKey
     * @param string $apiSecret
     *
     * @return $this
     */
    public static function make($apiKey, $apiSecret)
    {
        return new self($apiKey, $apiSecret);
    }

    /**
     * Make the Search API call.
     *
     * @param string $keyword
     *
     * @return ZttpResonse
     */
    public function search($keyword)
    {
        return $this->request([
            'method' => 'foods.search',
            'search_expression' => $keyword,
            'format' => 'json'
        ]);
    }

    /**
     * Get food details.
     *
     * @param int $foodId
     *
     * @return ZttpResonse
     */
    public function find($foodId)
    {
        return $this->request([
            'method' => 'food.get',
            'food_id' => $foodId,
            'format' => 'json'
        ]);
    }

    /**
     * Retrieve the Access Token.
     *
     * @return string
     */
    protected function refreshAccessToken()
    {
        $url = 'https://oauth.fatsecret.com/connect/token';
        $grantType = 'client_credentials';
        $scope = 'basic';
        $response = Zttp::asFormParams()
            ->withBasicAuth(
                $this->apiKey,
                $this->apiSecret
            )
            ->post($url, [
                'grant_type' => $grantType,
                'scope' => $scope,
            ]);

        return $this->accessToken = $response->json()['access_token'];
    }

    /**
     * Make the API request.
     *
     * @param array $parameters
     *
     * @return ZttpResponse
     */
    protected function request($parameters)
    {
        $token = $this->getToken();

        return Zttp::asFormParams()
            ->withHeaders([
                'Authorization' => "Bearer {$token}",
            ])
            ->post($this->baseUrl, $parameters);
    }

    /**
     * Get the Access Token.
     *
     * @return string
     */
    public function getToken()
    {
        if (!$this->accessToken) {
            $this->refreshAccessToken();
        }

        return $this->accessToken;
    }
}
