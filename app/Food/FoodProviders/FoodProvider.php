<?php

namespace App\Food\FoodProviders;

interface FoodProvider
{
    /**
     * Search by keyword on 3rd party food service provider.
     *
     * @param string $keyword
     *
     * @return Illuminate\Support\Collection
     */
    public function search($keyword);

    /**
     * Find Food on Food Provider with all the details.
     *
     * @param int $foodId
     *
     * @return App\Food\FoodProviders\Food
     */
    public function find($foodId);
}
