<?php

namespace App\Users;

use App\Goals\Goal;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use App\NutritionDiary\NutritionDiaryEntry;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Dont protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * define HasMany relationship.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nutritionDiaryEntries()
    {
        return $this->hasMany(NutritionDiaryEntry::class);
    }

    public function goal()
    {
        return $this->hasOne(Goal::class);
    }

    public function avatarUrl()
    {
        return $this->avatar
            ? Storage::disk('avatars')->url($this->avatar)
            : "https://www.gravatar.com/avatar/" . md5($this->email);
    }
}
