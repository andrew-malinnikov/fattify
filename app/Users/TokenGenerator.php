<?php

namespace App\Users;

use Illuminate\Support\Str;

class TokenGenerator
{
    public function generate()
    {
        $token = Str::random(60);

        if (User::whereApiToken($token)->exists()) {
            return $this->generate();
        }

        return $token;
    }
}
