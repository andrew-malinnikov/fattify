<?php

namespace App\Goals;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    /**
     * Don't protect against mass assignment
     * @var array
     */
    protected $guarded = [];
}
