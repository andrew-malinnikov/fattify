<?php

namespace App\Providers;

use App\Food\FoodProviders\FatSecretFoodProvider;
use App\Food\FoodProviders\FatSecretHttpClient;
use App\Food\FoodProviders\FoodProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        $this->registerFoodProvider();
    }

    public function boot()
    {
        \Debugbar::disable();
    }

    protected function registerFoodProvider()
    {
        $this->app->singleton(FoodProvider::class, function () {
            $client = FatSecretHttpClient::make(
                config('services.fatsecret.key'),
                config('services.fatsecret.secret')
            );

            return new FatSecretFoodProvider($client);
        });
    }
}
